﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WebClient
{
    static class Program
    {
        static readonly HttpClient client = new HttpClient();

        static async Task Main(string[] args)
        {            
            client.BaseAddress = new Uri("https://localhost:5001/");

            Console.WriteLine("Press Enter to continue:");
            Console.ReadLine();

            for (int i = 0; i<5; i++)
            {
                CustomerCreateRequest request = RandomCustomer();

                Console.WriteLine("New customer created:");
                Console.WriteLine("First name: " + request.Firstname);
                Console.WriteLine("Last name: " + request.Lastname);

                string json = JsonConvert.SerializeObject(request);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response_post = await client.PostAsync("customers", content);
                string responseBody_post = await response_post.Content.ReadAsStringAsync();

                Console.WriteLine("id: " + responseBody_post);
            }

            Console.WriteLine("Enter customer's id:");

            while (true)
            {
                var id = Console.ReadLine();

                HttpResponseMessage response_get = await client.GetAsync("customers/" + id);

                if (response_get.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    Console.WriteLine("Customer not found");
                }
                else
                {
                    string responseBody = await response_get.Content.ReadAsStringAsync();
                    Console.WriteLine(responseBody);
                }
            }            

            return;
        }

        private static CustomerCreateRequest RandomCustomer()
        {
            return new CustomerCreateRequest(GenerateRandomText(), GenerateRandomText());
        }

        private static string GenerateRandomText()
        {
            Random rnd = new Random();

            int FLength = rnd.Next(5, 10);
            string FirstName = "";

            for (int i = 0; i < FLength; i++)
            {
                FirstName += (char)rnd.Next(0x0410, 0x44F);
            }

            return FirstName;
        }
    }
}