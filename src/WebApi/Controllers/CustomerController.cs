using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Interfaces;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        private readonly ICustomersProvider customersProvider;

        public CustomerController(ICustomersProvider customersProvider)
        {
            this.customersProvider = customersProvider;
        }

        [HttpGet("{id:long}")]   
        public async Task<IActionResult> GetCustomerAsync([FromRoute] long id)
        {
            var result = await customersProvider.GetCustomerAsync(id);

            if (result.Customer != null)
            {
                return Ok(result.Customer);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost("")]   
        public async Task<IActionResult> CreateCustomerAsync([FromBody] Customer customer)
        {
            var id = await customersProvider.CreateCustomerAsync(customer.Firstname, customer.Lastname);

            if (id != 0)
            {
                return Ok(id);
            }
            else
            {
                return BadRequest();
            }
        }
    }
}