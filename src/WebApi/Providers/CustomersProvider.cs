﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Interfaces;
using WebApi.Models;

namespace WebApi.Providers
{
    public class CustomersProvider: ICustomersProvider
    {
        private readonly CustomersDbContext dbContext;

        public CustomersProvider(CustomersDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<(bool IsSuccess, Customer Customer, string ErrorMessage)> GetCustomerAsync(long id)
        {
            var customer = await dbContext.Customers.FirstOrDefaultAsync(it => it.Id == id);
            if (customer != null)
            {
                return (true, customer, null);
            }

            return (false, null, "Not found");
        }

        public async Task<long> CreateCustomerAsync(string Firstname, string Lastname)
        {
            try
            {
                var result = await dbContext.Customers.AddAsync(new Customer() { Firstname = Firstname, Lastname = Lastname });
                dbContext.SaveChanges();

                return result.Entity.Id;
            }
            catch
            {
                return 0;
            }
        }
    }
}
