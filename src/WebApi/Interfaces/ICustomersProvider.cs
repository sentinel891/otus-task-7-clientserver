﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Interfaces
{
    public interface ICustomersProvider
    {
        Task<(bool IsSuccess, Customer Customer, string ErrorMessage)> GetCustomerAsync(long id);

        Task<long> CreateCustomerAsync(string Firstname, string Lastname);
    }
}
